#ifndef STACK_HPP
#define STACK_HPP

#include <stdexcept>    // For std::range_error()
#include <iostream>     // For std::cout and std::endl

template <typename T>
class Stack{
    private:
        T ourNull{};    // A null for us to delete data with

        int count;      // Current number of items on the stack
        int size;       // The current size, or capacity, of the stack
        T *top;         // Pointer to the stack array 

        void swap( Stack<T> &left, Stack<T> &right );

        void grow();
        void shrink();

        void printStackHelper( int position );

    public:
        Stack();
        Stack( const Stack &rhs );
        virtual ~Stack();

        Stack &operator = ( Stack rhs );

        void clear();
        int getCount() const;
        bool isEmpty() const;

        //void emplace( const T data );
        void push( const T &newItem );
        T pop();
        T peek() const; 

        T peekBottom() const;
        void reverseStack();

        void printStack();
        //T getNull() const;
};

#include "stack.txx"

#endif

