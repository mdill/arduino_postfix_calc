#include "postfixCalc.hpp"

// <================================DELETE THIS
using std::cout;
using std::endl;
// <================================DELETE THIS

// Constructor
// Error flag is false, and which_error is NONE
postfixCalc::postfixCalc(){
    error_flag = false;
    which_error = NONE;
}

// Returns the status of error_flag
bool postfixCalc::check_error_flag() const{
    return error_flag;
}

//##################################################################################
// Parses the input stream and returns the result of the calculation
// Three errors:
// 1. Division by 0
// 2. Too few operands
// 3. Too few operations
double postfixCalc::compute( std::istream& in ){
    double answer = 0;

    std::string x;
    in >> x;

    while( !in.fail() ){
        if( x == "+" ){
            if( calc.get_count() < 2 ){
                which_error = TOO_FEW_OPERANDS;
                error_flag = true;
                throw std::runtime_error( "Too few operands" );
            }

            answer = add();
        }
        else if( x == "-" ){
            if( calc.get_count() < 2 ){
                which_error = TOO_FEW_OPERANDS;
                error_flag = true;
                throw std::runtime_error( "Too few operands" );
            }

            answer = sub();
        }
        else if( x == "*" ){
            if( calc.get_count() < 2 ){
                which_error = TOO_FEW_OPERANDS;
                error_flag = true;
                throw std::runtime_error( "Too few operands" );
            }

            answer = mul();
        }
        else if( x == "/" ){
            if( calc.get_count() < 2 ){
                which_error = TOO_FEW_OPERANDS;
                error_flag = true;
                throw std::runtime_error( "Too few operands" );
            }

            answer = div();
        }
        else if( x == "%" ){
            if( calc.get_count() < 2 ){
                which_error = TOO_FEW_OPERANDS;
                error_flag = true;
                throw std::runtime_error( "Too few operands" );
            }

            answer = mod();
        }
        else{
            calc.push( std::stoi( x ) );
            error_flag = false;
        }

        in >> x;
    }

    if( calc.get_count() > 1 ){
        which_error = TOO_FEW_OPERATIONS;
        error_flag = true;
        throw std::runtime_error( "Too few operations" );
    }

    return answer;
}

// Return the which_error enum
ERRORS postfixCalc::get_which_error() const{
    return which_error;
}

// Addition method
double postfixCalc::add(){
    double y = calc.peek();
    calc.pop();

    double x = calc.peek();
    calc.pop();

    calc.push( x + y );
    which_error = NONE;

    return (x + y);
}

// Subtraction method
double postfixCalc::sub(){
    double y = calc.peek();
    calc.pop();

    double x = calc.peek();
    calc.pop();

    calc.push( x - y );
    which_error = NONE;
    error_flag = false;

    return (x - y);
}

// Multiplication method
double postfixCalc::mul(){
    double y = calc.peek();
    calc.pop();

    double x = calc.peek();
    calc.pop();

    calc.push( x * y );
    which_error = NONE;
    error_flag = false;

    return (x * y);
}

// Division method
double postfixCalc::div(){
    double y = calc.peek();
    calc.pop();

    double x = calc.peek();
    calc.pop();

    if( y == 0 ){
        which_error = DIVISION_BY_ZERO;
        error_flag = true;
        throw std::runtime_error( "Division by zero" );
    }


    calc.push( static_cast<double>( x / y ) );
    which_error = NONE;
    error_flag = false;

    return (static_cast<double>( x / y ));
}

// Modulus method
double postfixCalc::mod(){
    double y = calc.peek();
    calc.pop();

    double x = calc.peek();
    calc.pop();

    calc.push( x % y );
    which_error = NONE;
    error_flag = false;

    return (x % y);
}

