# Arduino Selectable Relay

## Purpose

This simple sketch is designed to provide the user a postfix calculator via the
Serial Monitor.  By utilizing a built-in stack the user can input values and
operations to retrieve basic answers.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_postfix_calc.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_postfix_calc/src/master/LICENSE.txt) file for
details.

