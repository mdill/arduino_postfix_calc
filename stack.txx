// Constructor -- Creates a new stack of size 10
template <typename T>
Stack<T>::Stack(){
    size = 10;
    count = 0;

    top = new T[size];
}

// Copy constructor
template <typename T>
Stack<T>::Stack( const Stack &rhs ){
    size = rhs.size;
    count = rhs.count;

    top = new int[size];

    for( int i = 0; i < count; i++ )
        top[i] = rhs.top[i];
}

// Destructor
template <typename T>
Stack<T>::~Stack(){
    clear();

    delete [] top;
}

// Copy assignment operator
template <typename T>
Stack<T> &Stack<T>::operator = ( Stack<T> rhs ){
    swap( *this, rhs );

    return *this;
}

// Swap our new array
template <typename T>
void Stack<T>::swap( Stack<T> &left, Stack<T> &right ){
    std::swap( left.count, right.count );
    std::swap( left.size, right.size );
    std::swap( left.top, right.top );
}

// Deletel all entries on the stack
// Sets all private fields back to the default values
template <typename T>
void Stack<T>::clear(){
    for( int i = 0; i < size; i++ )
        top[i] = ourNull;

    delete [] top;

    size = 10;
    count = 0;

    top = new T[size];
}

// Returns the number of items in our stack
template <typename T>
int Stack<T>::getCount() const{
    return count;
}

// Returns if the stack is empty, or not
template <typename T>
bool Stack<T>::isEmpty() const{
    return ( count == 0 );
}

/*
// Constructs a new element, and pushes it onto the top of the stack
template <typename T>
void Stack<T>::emplace( const T data ){
    push( data );
}
*/

// Pushes a new item onto the stack
// Returns if the push was successful, or not
template <typename T>
void Stack<T>::push( const T &newItem ){
    count++;

    if( count == size )
        grow();

    top[count] = newItem;
}

// Grows our array when necessary
template <typename T>
void Stack<T>::grow(){
    T *temp = top;

    size *= 2;
    top = new T[size];

    for( int i = 0; i <= count; i++ ){
        top[i] = temp[i];
        temp[i] = ourNull;
    }

    delete [] temp;
}

// Pops an item off of the stack, then returns it for use
template <typename T>
T Stack<T>::pop(){
    if( count == 0 )
        throw std::range_error( "Cannot pop an empty stack!" );

    T ourItem = top[count];
    top[count] = ourNull;
    
    count--;

    if( count < size / 2 )
        shrink();

    return ourItem;
}

// Shrinks our array when possible
template <typename T>
void Stack<T>::shrink(){
    T *temp = top;

    size /= 2;
    top = new T[size];

    for( int i = 1; i <= count; i++){
        top[i] = temp[i];
        temp[i] = ourNull;
    }

    delete [] temp;
}

// Peeks at the top item of our stack without removing it
template <typename T>
T Stack<T>::peek() const{
    if( count == 0 )
        throw std::range_error( "Cannot peek an empty stack!" );

    return top[count];
}

// Peeks the "last" item at the bottom of the stack
template <typename T>
T Stack<T>::peekBottom() const{
    if( count == 0 )
        throw std::range_error( "Cannot peek an empty stack!" );

    return top[1];
}

// Reverses the entire stack, making the top the bottom, and the bottom the top
template <typename T>
void Stack<T>::reverseStack(){
    T *temp = top;

    top = new T[size];

    for( int i = 1; i <= count; i++ ){
        top[i] = temp[count - (i - 1)];
        temp[count - (i - 1)] = ourNull;
    }

    delete [] temp;
}

// Recursively prints the stack to see exactly what's inside
template <typename T>
void Stack<T>::printStack(){
    std::cout << "Top-> " << top[count];

    printStackHelper( count - 1 );

    std::cout << " <-Bottom" << std::endl << std::endl;
}

template <typename T>
void Stack<T>::printStackHelper( int position ){
    if( position != 0 ){
        T temp = top[position];
        std::cout << "  -  " << temp;

        printStackHelper( position - 1 );
    }

    return;
}

/*
template <typename T>
T Stack<T>::getNull() const{
    return ourNull;
}
*/

