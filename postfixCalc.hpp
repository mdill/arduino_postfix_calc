#ifndef POSTFIX_CALC_HPP
#define POSTFIX_CALC_HPP

#include "stack.hpp"
#include <iostream>
#include <string>

enum ERRORS { DIVISION_BY_ZERO, TOO_FEW_OPERANDS, TOO_FEW_OPERATIONS, NONE };
class postfixCalc{
    private:
        Stack<int> calc;    //the stack for the calculator
        bool error_flag;    //the error flag to indicate if an error occurred
        ERRORS which_error; //this indicate which, if any, error occurred

        double add();
        double sub();
        double mul();
        double div();
        double mod();
        //double sin();
        //double cos();
        //double tan();
        //double sqrt();
        //double log();
        //double ln();
        //double fact();

    public:
        postfixCalc();

        double compute( std::istream& in );

        bool check_error_flag() const;

        ERRORS get_which_error() const;
};
#endif

